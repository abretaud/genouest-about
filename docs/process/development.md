# Development process

All our developments fall in one of the following categories.

All projects progress are discussed during our meetings and tracked
in meeting notes.

## Project types

### Research and development

R&D are internal developments, targeting internal needs (though software could
be used by a wider audience).

Projects:

* are open source by default and should be published in our public repositories.
  If for any reason project should be private, it *must* be discussed during on
  of our regular meetings.
* have a license: Affero-GPL / Apache 2.0 / MIT
* have in entry in [Cesgo](https://www.cesgo.org/collaboration/), tagged as *R&D*
* follow our [R&D development workflow](#R&D)

### Collaboration

Those projects relate to collaboration requests where one is involved in
developments of a larger project, involving external or internal partners.
Partner may be involved in development or not (only providing existing
software, data, etc.)

Projects:

* have in entry in [Cesgo](https://www.cesgo.org/collaboration/),
  tagged as *R&D*
* have a license: to be decided with partners
* follow our [development workflow](#development)

### Web interface

Those projects relate to external or internal partners requesting the
development of a  *basic* development.
By *basic*, we mean projects requesting only a few days of work.

Projects:

* are private and in private repositories
* do not have a license usually
* follow our [light development workflow](#light)

## Workflow

### R&D

* project is declared in Cesgo as a R&D project and tracks all main
  project events
* use of git in Github or INRIA Gitlab infrastructure
* contains a README and LICENSE file
* provides INSTALL instructions
* has documentation
* provides unit tests and continuous integration
* if possible, provides Docker images
* if applicable, software is scalable (multi machine)

When in production, one must contact a system administrator to add backups

### Development

* customer requests for a new project with an helpesk support email
* meeting with the customer (with a license choice)
* project is approved during our meetings
* project is declared in Cesgo and tracks all main project events
* customer provides an input specification
  * Specification can be written by our team after the meeting and
    validated by the customer
* manage project in agile mode
* use of git in Github or INRIA Gitlab infrastructure
* contains a README and LICENSE file
* provides INSTALL instructions
* has documentation
* provides unit tests and continuous integration
* if possible, provides Docker images
* if applicable, software is scalable (multi machine)
* agree on regular releases for customer testing and validation
* in case of specification update, track requests in Cesgo and agree
  for a new planning.
* on project end, meeting with customer and track in Cesgo meeting notes
* customer sends the satisfaction form

If software is put in production in our infrastrcuture, one must contact
a system administrator to add backups

### Light

* customer requests for a new project with an helpesk support email
* meeting with the customer
* project is approved during our meetings
* customer provides an input specification
  * Specification can be written by our team after the meeting and
    validated by the customer
* use of git in Github or INRIA Gitlab infrastructure
* provides INSTALL instructions
* if possible, provides Docker images
* customer validates software and approval is registered via the helpdesk
  ticket
  * if not approved, track info in support email and agree needed updates
    and planning
* customer sends the satisfaction form

## Development constraints

There is no language constraints but Python, Node and Go are default choices.

If using Python, Python 3 should be used.

Docker should be used by default.

Commonly used frameworks:

* Node: Express
* Python: Pyramid/Flask
* Web: React, Angular

Web applications must be minified.

## Deploying in our infrastructure

All our servers are Linux based x86_64, so final application should only
target this architecture. Software *may* target other archs/OS but this
is not mandatory.

If not using Docker, software must run on CentOS 7, our current
operating system.

Our infrastructure provides shared directories between servers, they can be
used to share files for scalability or multi-component share.

Web accesses are restricted, all web servers communication must go through
our web proxy.

Applications *should* not run as root but as unprivileged user.

In case of authentication, software must use our LDAP server if applicable.
Elixir AAI can be used for a broader audience, allowing Elixir community to use
our tools.
