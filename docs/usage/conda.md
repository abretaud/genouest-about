# Conda

By default, the channels defaults, bioconda, conda-forge and r are available
on the cluster. The Bioconda channel in particular is tailored for
bioinformatics tools. You may add channel you need. Please keep in mind that
private channels might present security risks (software will not be vetted).
If possible, please keep to the standard channels.

[https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-channels.html](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-channels.html)

To use Conda, first source it the usual way (on a node):

    . /local/env/envconda.sh

With Conda, you can create as many environments as you want, each one
containing a list of packages you need. You need to activate an environment
to have access to software installed in it.
You can activate only one environment at a time.

To create a new environment containing biopython, deeptools (v2.3.4),
bowtie and blast, run:

    conda create -p ~/my_env biopython deeptools=2.3.4 bowtie blast

To activate it:

    conda activate ~/my_env

To deactivate it:

    conda deactivate

Feel free to test this new way to install software, and to give us feedback
whether you are happy or not of it.
