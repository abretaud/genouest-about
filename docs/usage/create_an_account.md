# Getting an account

To get an account, you should go to [Genouest account manager](https://my.genouest.org)
and click on *sign-in*.

Fill all needed info such as requested user identifier, contact information,
the reason of your request etc...

After form submission, you will receive an email requesting to confirm your
account request and click on the received email.

Now, you must wait... until an administrator approve your request. You may be
contacted if we feel something is wrong or missing in your request.

When your account is approved, you will receive an additional email with a few
information (how to connect, etc.).

At account approval, you are automatically subscribed to our
mailing list (low rate). This list is used to send general information
such as maintenance and service issues.

To unscribe or more information on our mailing [Genouest mailing list](../policy/mailing_list.md)
