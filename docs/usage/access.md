# Accessing our resources

## Connect

To connect to our cluster or other command line tools, you need to connect
to our bastion server *genossh.genouest.org*.

This is the entrypoint to our network. From there, you can submit jobs or
connect to other resources.

Access is done via SSH with an SSH key.

For Windows users, SSH can be done with the PuTTY tool for example, or using
the Linux subsystem of Windows.

* Create a RSA SSH key (ssh-keygen on linux, using PuTTYgen for windows)

  **Warning**: You key should be password protected!!!!
* Connect to our self-service portal
[https://my.genouest.org](https://my.genouest.org) and insert
your **public** key in the SSH section.
* Connect to genossh.genouest.org
* If you are prompted for a password, then it means your key was not accepted
(check previous steps first, then contact support)

To connect with PuTTY, you also must use Pageant to preload your private
key before the connection.

Once connected, to access other resources, you will still need your ssh key.

* either copy your private key on the cluster
    scp id_rsa *myuserid*@genossh:~/.ssh/
* or allow agent forwarding (in PuTTY: session option *Allow agent forwarding*,
with ssh: ssh -A ...)

## Copy data

It is possible to copy data from/to cluster via the scp tool.

An FTP server is also available:

* host: <ftps://gridftp.genouest.org>
* port: 990
* login: [your-genouest-login]
* password: [your-genouest-password]

You can use any ftp compliant tool to transfer data, be sure however to use the
ftps (secure) option and not ftp
and to specify port 990.
