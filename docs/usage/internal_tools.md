# Our internal tools

## Daily use tools

To manage our facility, we use many differents tools you should know as a
Genouest member.

### Helpdesk

Our helpdesk is hosted at [https://support.genouest.org](https://support.genouest.org)

It is a ticket system used to manage our user issues. All Genouest team members
*can* and **should**, whatever his primary job, connect to the system and see if
a can help to close a ticket.

We use different *queues* following our quality management
(tools, information system, expertise, etc.), but as a team we all try to be as
collaborative as possible. Whatever is your expertise, you always can help to answer
user questions, do analysis on an issue or help in triage.

It is possible to follow specific queues.

After your account creation, you are automatically registered to follow all tickets,
and will receive related emails. You can connect to the system if you wish to
unsubscribe to some (or all) queues.

### Internal communication

Beside traditional email, we push the use of our instant messaging tool [RocketChat](https://instant.cesgo.org/home).
We have a primary internal channel (Genouest) but also some more dedicated
channels for our internal communication. It eases collaboration as all channel
members can see and answer your questions. And anyone can follow the current
issues even if not directly impacted.

For private communication or communication involving external partners, you
should use the email, copying involved participants. Try to keep in copy of the
conversation all participants, unless explicitly requested.

Email being an asynchronous communication tool, do not expect an immediate
answer from your colleagues! Be kind and wait for his answer, he may be on
travel, doing remote work, have more important matters to manage or simply
sleeping if you are a work-addict working at night (in this last case, you
should refer to the internal regulation document about office hours).

## Development

Regarding development, we primarly use:

* [GitHub Genouest repository](https://github.com/genouest/) for public projects
* [INRIA Gitlab repository](https://gitlab.inria.fr/genouest) for private projects

Newcomers should ask to be added to the Genouest team of both of those repositories

## Storage

To manage storage quotas and creation, see [storage process](https://gitlab.inria.fr/genouest/genouest-admin-private/blob/master/storage/about.md)
(internal access only).

You need admin rights to access servers.
