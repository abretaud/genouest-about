# BioMAJ

Some databanks are available in the /db directory.

They are automatically updated and indexed on a regular basis using
[BioMAJ](https://biomaj.genouest.org/).
Some banks are available in multiple versions. The current symbolic link targets
the latest version available on our servers.

You can consult the list of available banks on our
[BioMAJ instance](https://banks.genouest.org/app/#/bank).

When working on /db, do not use in your scripts the /db/..../current path,
always look at targeted directory to use its real path as bank may be updated
during your job. You can get real path with:

    readlink -f /db/genbankRelease/current/

Need something not available, just send us an email to the support team
and we will see what we can do for you...
